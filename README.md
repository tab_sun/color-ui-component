# ColorUI Pro

#### 介绍
基于ColorUI组件库开发，在基础之上，封装成了ColorUI小程序组件版本，使代码更简洁，更强壮，并添加了自己的扩展。实现了消息提示框，顶部提示框，圆形进度条，骨架屏，表单效验，动态表单，瀑布流，日历，全屏弹幕，全屏点赞，手风琴，探探滑动，富文本，拖拽列表，长列表等等。

#### 图片资源：
链接: https://pan.baidu.com/s/1M83k42FQq6-W9qr1ehkmUw 提取码: hncy

#### 项目截图
![输入图片说明](introduce/1.png)
![输入图片说明](introduce/10.png)
![输入图片说明](introduce/4.png)
![输入图片说明](introduce/2.png)
![输入图片说明](introduce/3.png)
![输入图片说明](introduce/5.png)
![输入图片说明](introduce/6.png)
![输入图片说明](introduce/7.png)
![输入图片说明](introduce/8.png)
![输入图片说明](introduce/9.png)
