Component({
  options: {
    addGlobalClass: true,
  },
  data: {
    elements: [
      { title: '操作条', name: 'bar', color: 'purple', icon: 'vipcard' },
      { title: '导航栏 ', name: 'nav', color: 'mauve', icon: 'formfill' },
      { title: '列表', name: 'list', color: 'pink', icon: 'list' },
      { title: '卡片', name: 'card', color: 'brown', icon: 'newsfill' },
      { title: '表单', name: 'form', color: 'red', icon: 'formfill' },
      { title: '时间轴', name: 'timeline', color: 'orange', icon: 'timefill' },
      { title: '聊天', name: 'chat', color: 'green', icon: 'messagefill' },
      { title: '轮播', name: 'swiper', color: 'olive', icon: 'album' },
      { title: '模态框', name: 'modal', color: 'grey', icon: 'squarecheckfill' },
      { title: '步骤条', name: 'steps', color: 'cyan', icon: 'roundcheckfill' },
      { title: '表单效验', name: 'rule-input', color: 'mauve', icon: 'ticket' },
      { title: '表单扩展', name: 'form-pro', color: 'purple', icon: 'similar' },
      { title: '动态表单', name: 'dynamic-input', color: 'pink', icon: 'form' },
      { title: '评分', name: 'rate', color: 'brown', icon: 'upstage' },
      // { title: '数量选择器', name: 'steps', color: 'red', icon: 'rank' },
      { title: 'tooltip提示框', name: 'tooltip', color: 'orange', icon: 'mark' },
      // { title: '图片上传', name: 'upload', color: 'olive', icon: 'upload' },
      { title: '颜色拾取器', name: 'color-picker', color: 'green', icon: 'filter' },
      { title: '双向滑动', name: 'double-slider', color: 'olive', icon: 'album' },
    ],
  },
  lifetimes:{
    attached(){
      this.animation();
    }
  },
  methods:{
    animation(){
      this.setData({
        toggleDelay: true
      })
      setTimeout(() => {
        this.setData({
          toggleDelay: false
        })
      }, 3000)
    }
  },
})
