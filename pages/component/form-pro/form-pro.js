const app = getApp();
Page({
  data: {
    show: false,
    show1: false,
    input: '',
    input1: '',
    color: 'red',
    ColorList: app.globalData.ColorList,
  },

  onLoad: function (options) {

  },

  setShow(){
    this.setData({
      show: true
    })
  },
  setShow1(){
    this.setData({
      show1: true
    })
  },
  click(e){
    const {year,month,today,week} = e.detail;
    let input = `${year}年${month}月${today}日`
    this.setData({input,show:false});
  },

  SetColor(e) {
    this.setData({
      color: e.currentTarget.dataset.name,
      show1: false,
      input1:  e.currentTarget.dataset.color,
    })
  },
})