// pages/component/rule-input/rule-input.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    input1: ""
  },

  onLoad: function (options) {

  },
  onReady() {
    this.input1 = this.selectComponent("#input1");
    this.input2 = this.selectComponent("#input2");
    this.input3 = this.selectComponent("#input3");
    this.input4 = this.selectComponent("#input4");
    this.input5 = this.selectComponent("#input5");
    this.toast = this.selectComponent("#toast");
  },
  submit() {
    const {input1,input2,input3,input4,input5} = this.data;
    if(!this.input1.check('regChinese')){
      this.toast.setShow("error","请输入中文");
      return;
    };
    if(!this.input2.check('regPhone')){
      this.toast.setShow("error","请输入正确电话");
      return;
    };
    if(!this.input3.check('regEmail')){
      this.toast.setShow("error","请输入正确邮箱");
      return;
    };
    if(!this.input4.check('regIdCard')){
      this.toast.setShow("error","请输入正确身份证");
      return;
    };
    if(!this.input5.check('regRequire')){
      this.toast.setShow("error","数据不能为空");
      return;
    };
    this.toast.setShow("success","输入成功");
  }
})