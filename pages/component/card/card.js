const app = getApp();
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    case: [
      {
        tagName: '史诗',
        tagColor: 'blue',
        title: "我已天理为凭，踏入这片荒芜，不再受凡人的枷锁遏制。我已天理为凭，踏入这片荒芜，不再受凡人的枷锁遏制。",
        name: "正义天使 凯尔",
        image: "https://ossweb-img.qq.com/images/lol/web201310/skin/big10006.jpg",
        avatar: "https://ossweb-img.qq.com/images/lol/web201310/skin/big10006.jpg",
        time: "十天前",
        tags: [
          {
            name: "attentionfill",
            num: 10
          },
          {
            name: "appreciatefill",
            num: 20
          },
          {
            name: "messagefill",
            num: 30
          },
        ]
      },
      {
        tagName: '史诗',
        tagColor: 'blue',
        title: "我已天理为凭，踏入这片荒芜，不再受凡人的枷锁遏制。我已天理为凭，踏入这片荒芜，不再受凡人的枷锁遏制。",
        name: "正义天使 凯尔",
        image: "https://ossweb-img.qq.com/images/lol/web201310/skin/big10006.jpg",
        avatar: "https://ossweb-img.qq.com/images/lol/web201310/skin/big10006.jpg",
        time: "十天前",
        tags: [
          {
            name: "attentionfill",
            num: 10
          },
          {
            name: "appreciatefill",
            num: 20
          },
          {
            name: "messagefill",
            num: 30
          },
        ]
      },
    ],
    dynamic: [
      {
        content: "我已天理为凭，踏入这片荒芜，不再受凡人的枷锁遏制。我已天理为凭，踏入这片荒芜，不再受凡人的枷锁遏制。",
        name: "正义天使 凯尔",
        image: "https://ossweb-img.qq.com/images/lol/web201310/skin/big10006.jpg",
        avatar: "https://ossweb-img.qq.com/images/lol/web201310/skin/big10006.jpg",
        time: "2019年12月3日",
        tags: [
          {
            name: "attentionfill",
            num: 10
          },
          {
            name: "appreciatefill",
            num: 20
          },
          {
            name: "messagefill",
            num: 30
          },
        ],
        message: [
          {
            avatar: 'https://ossweb-img.qq.com/images/lol/img/champion/Morgana.png',
            name: "莫甘娜",
            comment: "凯尔，你被自己的光芒变的盲目。",
            replyName: "凯尔",
            reply: "妹妹，你在帮他们给黑暗找借口吗",
            time: "2018年12月4日",
            tags: [{
                name: "appreciatefill",
                num: 20
              },{
                name: "messagefill",
                num: 30
              },
            ],
          },
          {
            avatar: 'https://ossweb-img.qq.com/images/lol/img/champion/Morgana.png',
            name: "莫甘娜",
            comment: "凯尔，你被自己的光芒变的盲目。",
            replyName: "凯尔",
            reply: "妹妹，你在帮他们给黑暗找借口吗",
            time: "2018年12月4日",
            tags: [{
                name: "appreciatefill",
                num: 20
              },{
                name: "messagefill",
                num: 30
              },
            ],
          }
        ]
      },
    ],
    article: [
      {
        title: "无意者 烈火焚身;以正义的烈火拔出黑暗。我有自己的正义，见证至高的烈火吧。",
        content: "折磨生出苦难，苦难又会加剧折磨，凡间这无穷的循环，将有我来终结！真正的恩典因不完整而美丽，因情感而真诚，因脆弱而自由！",
        image: "https://ossweb-img.qq.com/images/lol/web201310/skin/big10006.jpg",
        tags: [
          {
            name: "正义天使",
            bgColor: "red"
          },
          {
            name: "史诗",
            bgColor: "green"
          }
        ]
      },
      {
        title: "无意者 烈火焚身;以正义的烈火拔出黑暗。我有自己的正义，见证至高的烈火吧。",
        content: "折磨生出苦难，苦难又会加剧折磨，凡间这无穷的循环，将有我来终结！真正的恩典因不完整而美丽，因情感而真诚，因脆弱而自由！",
        image: "https://ossweb-img.qq.com/images/lol/web201310/skin/big10006.jpg"
      }
    ]
  },
  isCard(e) {
    this.setData({
      isCard: e.detail.value
    })
  },
});
