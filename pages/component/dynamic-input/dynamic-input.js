Page({
  data: {
    name: '',
    temp: '',
    content: [
      //   {
      //     title: '标题',
      //     question: ['xx',
      //     'xx',
      //     'xx',
      //     'xx',
      //     'xx',
      //     ],
      //     other: [
      //       '其他'
      //     ]
      //   }
    ]
  },

  onReady() {
    this.toast = this.selectComponent("#toast");
  },

  finish() {
    console.log(this.data.content)
    wx.navigateBack({
      delta: 1,
    })
  },
  otherChange(event){
    let index = event.currentTarget.dataset.index;
    console.log(index)
    let content = this.data.content;
    content[index].other = event.detail.value
    console.log(content)
    this.setData({
      content
    })
  },
  multiSelectChange(event){
    let index = event.currentTarget.dataset.index;
    console.log(index)
    let content = this.data.content;
    content[index].more = event.detail.value
    console.log(content)
    this.setData({
      content
    })
  },
  addTitle() {
    if (this.data.temp == '') {
      this.toast.setShow("error","请输入标题");
      return
    }
    let data = {
      title: this.data.temp,
      question: [

      ],
      other: false,
      more: false
    }
    let content = this.data.content;
    content.push(data)
    this.setData({
      content,
      temp: ''
    })
  },
  deleteTitle(event) {
    let index = event.currentTarget.dataset.index;
    console.log(index)
    let content = this.data.content;
    content.splice(index, 1)
    this.setData({
      content
    })
  },
  addQuestion(event) {
    console.log(event)
    let index = event.currentTarget.dataset.index;
    let content = this.data.content;
    content[index].question.push("")
    this.setData({
      content
    })
    console.log(this.data.content)
    // let question = content[index].question;1
    // console.log(question.length)
    // if(index == 0){
    //   this.setData({
    //     ['content[' + (index) + '].question']: [""]
    //   })
    // }else{
    //   this.setData({
    //     ['content[' + (question.length) + '].question']: ""
    //   })
    // }
    console.log(content)
  },
  deleteQuestion(event) {
    let index = event.currentTarget.dataset.index;
    let idx = event.currentTarget.dataset.idx;
    let content = this.data.content;
    content[index].question.splice(idx, 1)
    this.setData({
      content
    })
  },
  setInput(event) {
    let index = event.currentTarget.dataset.index;
    let idx = event.currentTarget.dataset.idx;
    let value = event.detail.value;
    let content = this.data.content;
    console.log(index, idx)
    content[index].question[idx] = value;
    console.log(content)
    this.setData({
      content
    })
  }
})