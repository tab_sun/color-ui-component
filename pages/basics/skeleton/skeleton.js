// pages/basics/skeleton/skeleton.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loading: false,
    data: [{
        tagName: '唯美',
        tagColor: 'blue',
        light: true,
        title: "嗷呜1~",
        name: "LeeYaMaster",
        image: "cloud://could-3gpmtjxfc5eed68f.636f-could-3gpmtjxfc5eed68f-1304798122/小清新图片/1.jpg",
        avatar: "cloud://could-3gpmtjxfc5eed68f.636f-could-3gpmtjxfc5eed68f-1304798122/1.jpg",
        time: "哦嗨哟 sama"
      },
      {
        tagName: '唯美',
        tagColor: 'blue',
        light: true,
        title: "嗷呜2~",
        name: "LeeYaMaster",
        image: "cloud://could-3gpmtjxfc5eed68f.636f-could-3gpmtjxfc5eed68f-1304798122/小清新图片/2.jpg",
        avatar: "cloud://could-3gpmtjxfc5eed68f.636f-could-3gpmtjxfc5eed68f-1304798122/1.jpg",
        time: "哦嗨哟 sama"
      },
      {
        tagName: '唯美',
        tagColor: 'blue',
        light: true,
        title: "嗷呜3~",
        name: "LeeYaMaster",
        image: "cloud://could-3gpmtjxfc5eed68f.636f-could-3gpmtjxfc5eed68f-1304798122/小清新图片/3.jpg",
        avatar: "cloud://could-3gpmtjxfc5eed68f.636f-could-3gpmtjxfc5eed68f-1304798122/1.jpg",
        time: "哦嗨哟 sama"
      },
      {
        tagName: '唯美',
        tagColor: 'blue',
        light: true,
        title: "嗷呜4~",
        name: "LeeYaMaster",
        image: "cloud://could-3gpmtjxfc5eed68f.636f-could-3gpmtjxfc5eed68f-1304798122/小清新图片/4.jpg",
        avatar: "cloud://could-3gpmtjxfc5eed68f.636f-could-3gpmtjxfc5eed68f-1304798122/1.jpg",
        time: "哦嗨哟 sama"
      }
    ],
    article: [
      {
        title: "无意者 烈火焚身;以正义的烈火拔出黑暗。我有自己的正义，见证至高的烈火吧。",
        content: "折磨生出苦难，苦难又会加剧折磨，凡间这无穷的循环，将有我来终结！真正的恩典因不完整而美丽，因情感而真诚，因脆弱而自由！",
        image: "https://ossweb-img.qq.com/images/lol/web201310/skin/big10006.jpg",
        tags: [
          {
            name: "正义天使",
            bgColor: "red"
          },
          {
            name: "史诗",
            bgColor: "green"
          }
        ]
      },
      {
        title: "无意者 烈火焚身;以正义的烈火拔出黑暗。我有自己的正义，见证至高的烈火吧。",
        content: "折磨生出苦难，苦难又会加剧折磨，凡间这无穷的循环，将有我来终结！真正的恩典因不完整而美丽，因情感而真诚，因脆弱而自由！",
        image: "https://ossweb-img.qq.com/images/lol/web201310/skin/big10006.jpg"
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  click1() {
    this.setData({
      loading: !this.data.loading
    })
  }
})