Page({
  data: {

  },
  onLoad(options) {

  },
  onReady() {
    this.toast = this.selectComponent("#toast");
    this.toast1 = this.selectComponent("#toast1");
    this.toast2 = this.selectComponent("#toast2");
  },
  click1() {
    this.toast.setShow("success", "提交成功，我们将在1个工作日内通知您");
  },
  click2() {
    this.toast.setShow("error","提交失败");
  },
  click3() {
    this.toast1.setShow("success","时间为5秒",5000);
  },
  click4() {
    this.toast2.setShow("success","提交成功，我们将在1个工作日内通知您");
  },
  end(){
    wx.showToast({
      title: '触发回调方法',
    })
  }
})