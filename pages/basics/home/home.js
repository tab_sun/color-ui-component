Component({
  options: {
    addGlobalClass: true,
  },
  data: {
    elements: [{
        title: '布局',
        name: 'layout',
        color: 'cyan',
        icon: 'newsfill'
      },
      {
        title: '背景',
        name: 'background',
        color: 'blue',
        icon: 'colorlens'
      },
      {
        title: '文本',
        name: 'text',
        color: 'purple',
        icon: 'font'
      },
      {
        title: '图标 ',
        name: 'icon',
        color: 'mauve',
        icon: 'icon'
      },
      {
        title: '按钮',
        name: 'button',
        color: 'pink',
        icon: 'btn'
      },
      {
        title: '标签',
        name: 'tag',
        color: 'brown',
        icon: 'tagfill'
      },
      {
        title: '头像',
        name: 'avatar',
        color: 'red',
        icon: 'myfill'
      },
      {
        title: '进度条',
        name: 'progress',
        color: 'orange',
        icon: 'icloading'
      },
      {
        title: '边框阴影',
        name: 'shadow',
        color: 'olive',
        icon: 'copy'
      },
      {
        title: '加载',
        name: 'loading',
        color: 'green',
        icon: 'loading2'
      },
      {
        title: '消息弹出框',
        name: 'toast',
        color: 'cyan',
        icon: 'message'
      },
      {
        title: '顶部提示框',
        name: 'toast-top',
        color: 'blue',
        icon: 'top'
      },
      // {
      //   title: '圆形进度条',
      //   name: 'circle',
      //   color: 'purple',
      //   icon: 'icloading'
      // },
      {
        title: '骨架屏',
        name: 'skeleton',
        color: 'mauve',
        icon: 'icloading'
      },
      {
        title: '页底提示',
        name: 'loadmore',
        color: 'brown',
        icon: 'pulldown'
      },
      // {
      //   title: '通告栏',
      //   name: 'loadmore',
      //   color: 'red',
      //   icon: 'info'
      // },
      {
        title: '状态展示',
        name: 'empty',
        color: 'pink',
        icon: 'sort'
      },
      // {
      //   title: '完整列表',
      //   name: 'list',
      //   color: 'orange',
      //   icon: 'list'
      // }
    ],
  },
  lifetimes:{
    attached(){
      this.animation();
    }
  },
  methods:{
    animation(){
      this.setData({
        toggleDelay: true
      })
      setTimeout(() => {
        this.setData({
          toggleDelay: false
        })
      }, 3000)
    }
  },
})