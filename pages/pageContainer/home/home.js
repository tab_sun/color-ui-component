Component({
  options: {
    addGlobalClass: true,
  },
  data: {
    toggleDelay: false,
    elements: [
      {
        title: '瀑布流',
        name: 'waterfall-flow',
        color: 'olive',
        icon: 'similar'
      },
      // {
      //   title: '表单扩展',
      //   name: 'form-pro',
      //   color: 'mauve',
      //   icon: 'comment'
      // },
      {
        title: '日历',
        name: 'calendar',
        color: 'purple',
        icon: 'calendar'
      },
      {
        title: '全屏弹幕',
        name: 'full-danmu',
        color: 'pink',
        icon: 'community'
      },
      {
        title: '全屏点赞',
        name: 'full-like',
        color: 'green',
        icon: 'like'
      },
      {
        title: '导航栏扩展',
        name: 'nav-pro',
        color: 'orange',
        icon: 'roundrightfill'
      },
      // {
      //   title: '全屏抖音',
      //   name: 'fill-video',
      //   color: 'purple',
      //   icon: 'video'
      // },
      {
        title: '金刚区',
        name: 'kingkong-area',
        color: 'mauve',
        icon: 'similar'
      },
      {
        title: '瓷片区',
        name: 'creamic-area',
        color: 'cyan',
        icon: 'similar'
      },
      {
        title: '表格',
        name: 'table',
        color: 'pink',
        icon: 'list'
      },
      {
        title: '后台表格',
        name: 'admin-table',
        color: 'brown',
        icon: 'list'
      },
      { title: '手风琴', name: 'piano', color: 'purple', icon: 'vipcard' },
      { title: '探探', name: 'tantan', color: 'mauve', icon: 'picfill' },
      // { title: '探探商品', name: 'tantan-shop', color: 'olive', icon: 'album' },
      // { title: '贝塞尔曲线', name: 'bessel', color: 'pink', icon: 'light' },
      // { title: '视频剪辑', name: 'video-clip', color: 'brown', icon: 'videofill' },
      { title: '官方富文本', name: 'editor', color: 'red', icon: 'edit' },
      { title: '我的富文本', name: 'my-editor', color: 'green', icon: 'edit' },
      { title: '新闻', name: 'posts', color: 'orange', icon: 'news' },
      { title: '拖拽列表', name: 'drag-list', color: 'cyan', icon: 'formfill' },
      // { title: '骨架屏', name: 'skeleton', color: 'olive', icon: 'timefill' },
      { title: '长列表', name: 'long-list', color: 'purple', icon: 'vipcard' },
      { title: '表情', name: 'emoji', color: 'mauve', icon: 'formfill' },
      { title: '粘性定位', name: 'sticky', color: 'grey', icon: 'squarecheckfill' },
      // { title: '新手指引页面', name: 'emoji', color: 'mauve', icon: 'formfill' },
    ],
  },
  lifetimes:{
    attached(){
      this.animation();
    }
  },
  methods:{
    animation(){
      this.setData({
        toggleDelay: true
      })
      setTimeout(() => {
        this.setData({
          toggleDelay: false
        })
      }, 3000)
    }
  },
  upper(){
    console.log("触发upper")
  },
  lower(){
    console.log("触发lower")
  },

})
