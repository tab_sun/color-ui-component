Page({

  data: {
    list: [],      // 数据源
    windowHeight: 0,      // 屏幕高度
    scrollItemHeight: 0,  // 列表单项高度
    bottomHeight: 0,      // 底部按钮高度
    selectItem: {},       // 当前选中元素
    selectIndex: 0,       // 当前选中索引
    editting: false,      // 是否是“编辑”状态
    moveData: null,       // 列表项移动时记录移动位置
    scrollTop: 0,          // scroll-view距离顶部距离
    list: [
      
    ]
  },

  onLoad: function () {
    this.init()
  },

  // 初始化页面数据
  init() {
    let list = this.data.list;
    for(let i = 1;i<20;i++){
      let obj = {
        name: "LeeYaMaster",
        avatar: "cloud://could-3gpmtjxfc5eed68f.636f-could-3gpmtjxfc5eed68f-1304798122/1.jpg",
        time: "LeeYa sama" + i,
      }
      list.push(obj)
    }
    this.setData({
      list,
      windowHeight: wx.getSystemInfoSync().windowHeight
    }, () => {
      wx.createSelectorQuery().select('.index-bottom').boundingClientRect(res => {
        this.setData({
          bottomHeight: res.height
        })
      }).exec()
      wx.createSelectorQuery().select('.scroll-item').boundingClientRect(res => {
        this.setData({
          scrollItemHeight: res.height
        })
      }).exec()
    });
  },

  // 开始拖拽
  scrollTouchStart(event) {
    let scrollItemHeight = this.data.scrollItemHeight;
    const firstTouchPosition = {
      x: event.changedTouches[0].pageX,
      y: event.changedTouches[0].pageY,
    }
    console.log(firstTouchPosition)
    const { data, index } = this.getPositionDomByXY(firstTouchPosition);
    console.log(data,index)
    this.setData({
      moveData:{
        x: 0,
        y: firstTouchPosition.y - scrollItemHeight / 2
      },
      selectItem: data,
      selectIndex: index
    })

  },

  // 拖拽ing...
  scrollTouchMove(event) {
    const { f } = this.data
    this.setData({
      moveData:{
        x: 0,
        y: event.changedTouches[0].pageY - this.data.scrollItemHeight / 2
      },
    })

  },

  // 拖拽结束
  scrollTouchEnd:function (event) {
    const { selectIndex, list } = this.data
    const endTouchPosition = {
      x: event.changedTouches[0].pageX,
      y: event.changedTouches[0].pageY,
    }
    const { index } = this.getPositionDomByXY(endTouchPosition)
    // 交换顺序
    const temp = list[selectIndex]
    list[selectIndex] = list[index]
    list[index] = temp
    this.setData({
      list,
      moveData: null
    })
  },

  // 根据(x,y)坐标轴获取页面元素
  getPositionDomByXY(potions) {
    const { scrollItemHeight, list, scrollTop } = this.data
    const y = potions.y + scrollTop;
    const len = list.length
    for(let i = 0; i < len; i++){
      if(y >= i*scrollItemHeight && y < (i+1)*scrollItemHeight){
        // 返回匹配到的数据项
        return {
          data: list[i],
          index: i
        };
      }
    }
    return y > (len-1)*scrollItemHeight ? {
      // 匹配项位于列表之下
      data: list[len - 1],
      index: len - 1
    } : {
      // 匹配项位于列表之上
      data: list[0],
      index: 0
    }
  },

  // 切换编辑状态
  changeEditing() {
    const { editting } = this.data
    this.setData({ editting: !editting })
  },

  // 监听滚动事件
  bindscroll(e) {
    this.data.scrollTop = e.detail.scrollTop
  }

})
