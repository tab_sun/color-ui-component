// 长列表这功能属于我自己原创的，用了很多种思路，踩了很多坑。
// 第一种：把1000条数据放在Page外面，然后把显示的100条数据放在Page里面，通过
// upper-threshold="1500" lower-threshold="1500" bindscrolltolower="lower" 
// bindscrolltoupper="upper"判断用户上滑还是下滑，来进行追加,但是这种效果不行，因为用户猛滑，方法会失灵
// 第二种：也是把1000条数据放在外面，通过Scroll滚动来显示数据，这种方法可行，
// 但是我觉得这方法还可以更加优化，因为每次判断上滑下滑，总觉得还有更好的方式。
// 第三种，也是我现在的办法，把list弄成一个二维数组，然后WXML不用For循环，写四个元素，根据
// 下标来显示，然后我通过滚动来改变下标即可实现，更改下标的时候，赋值不要用i + 1
// 直接用滑动的距离除以元素高度再除以一个二维数组里有多少个元素，再赋值就实现此功能了，并且这方法还不用判断上滑下滑。
Page({
  data: {
    list: [],
    i: 0
  },

  onLoad: function (options) {
    this.init()
  },
  init() {
    let list = [];
    for (let i = 1; i <= 1000; i++) {
      let obj = {
        name: "LeeYaMaster",
        avatar: "cloud://could-3gpmtjxfc5eed68f.636f-could-3gpmtjxfc5eed68f-1304798122/1.jpg",
        time: "LeeYa sama" + i,
      }
      list = [...list, obj];
    }
    list = this.format(list);
    console.log(list)
    this.setData({
      list
    })
  },
  format(data) {
    // 1000条数据，将25条分为一组，之后形成一个二维数组
    const count = data.length / 25;
    let list = [];
    for (let i = 0; i < data.length; i += count) {
      let obj = {
        i, // 这里的i可有可无，只是为了方便看
        data: data.slice(i, i + count)
      }
      list = [...list, obj];
    }
    console.log(list)
    return list;
  },

  scroll(event) {
    const {
      scrollLeft,
      scrollTop,
      scrollHeight,
      scrollWidth,
      deltaX,
      deltaY
    } = event.detail;
    console.log(scrollTop);
    // 一个item高度大约为50,itemCount为往下划走了多少个item
    const itemCount = scrollTop / 50;
    if(Math.floor(itemCount / 10) > 21){
      return 
    }
    this.setData({
      i: Math.floor(itemCount / 10)
    })
    console.log(this.data.i);
  }
})