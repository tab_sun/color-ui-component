// pages/component/kingkong-area/kingkong-area.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  onLoad() {
    let list = [];
    for (let i = 1; i <= 30; i++) {
      let obj = {
        cover: `cloud://could-3gpmtjxfc5eed68f.636f-could-3gpmtjxfc5eed68f-1304798122/kingkong/${i}.png`,
        text: `文字${i}`
      }
      list.push(obj)
    }
    list = [...list]
    this.setData({
      list
    })
  }
})