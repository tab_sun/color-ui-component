Page({

  data: {
    header: [{
      name: '序号',
      score: '题目',
    }],
    score: [{
      name: "重庆大学",
      address: '重庆市',
      tag: [985, 211],
      detail: [{
          year: 2019,
          top_rank: 3499,
          low_rank: 3579,
          avg_rank: 3895
        },
        {
          year: 2018,
          top_rank: 3499,
          low_rank: 3579,
          avg_rank: 3895
        }
      ]
    },
    {
      name: "四川大学",
      address: '成都市',
      tag: [985, 211],
      detail: [{
          year: 2019,
          top_rank: 3499,
          low_rank: 3579,
          avg_rank: 3895
        },
        {
          year: 2018,
          top_rank: 3499,
          low_rank: 3579,
          avg_rank: 3895
        }
      ]
    }
  ]
  },

  onLoad() {
    let list = [];
    for (let i = 1; i <= 20; i++) {
      let obj = {
        number: '分数',
        score: i
      }
      list.push(obj)
    }
    this.setData({
      list
    })
  },

})