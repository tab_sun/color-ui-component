var WxParse = require('../../../utils/wxParse/wxParse.js');
let page = 0;
Page({
  data: {
    posts: [],
    search: '',
    // 弹框
    post: {},
    transformIdx: 0,
    position: 'center',
    duration: 300,
    show: false,
    overlay: false
  },
  onLoad(options) {
    this.getPosts();
  },
  getPosts() {
    let list = [];
    for (let i = 1; i <= 6; i++) {
      let obj = {
        cover: `cloud://could-3gpmtjxfc5eed68f.636f-could-3gpmtjxfc5eed68f-1304798122/二次元/${i}.jpg`,
        title: '文章演示标题',
        content: '清明假期2021年4月3-4日，北京国家会议中心，中国北方规模超大、观众超多、coser超多、超火爆的二次元大型跨年狂欢节——IJOY北京国际动漫游戏狂欢节 × CGF中国游戏节要和各位小伙伴们见面啦！清明假期的特大二次元跨年狂欢庆典，来IJOY...',
      }
      list.push(obj)
    }
    list = this.format(list);//将富文本转换为文字
    let post = this.data.posts;
    let posts = [...post, ...list]
    this.setData({
      posts
    })
  },
  format(data) {
    data.forEach((item) => {
      item.content_parse = item.content;
      item.content_parse = item.content_parse.replace(/(\n)/g, "");
      item.content_parse = item.content_parse.replace(/(\t)/g, "");
      item.content_parse = item.content_parse.replace(/(\r)/g, "");
      item.content_parse = item.content_parse.replace(/<\/?[^>]*>/g, "");
      item.content_parse = item.content_parse.replace(/\s*/g, "");
    })
    return data;
  },

  onReachBottom() {
    console.log("下拉刷新")
    page = page + 1;
    this.getPosts();
  },
  enter() {
    // let posts = await wx.cloud.callFunction({
    //   name: 'posts',
    //   data: {
    //     action: 'search',
    //     search: this.data.search
    //   }
    // })
    // posts = posts.result.data;
    // posts.forEach(item => {
    //   item.createTime = this.formatTime(item.createTime);
    // })
    // posts = this.format(posts);
    // this.setData({
    //   posts
    // })
  },
  search(e) {
    if (e.detail.value == '') {
      page = 0;
      this.setData({posts:[]})
      this.getPosts();
    }
  },
  formatTime(data) {
    let date = new Date(data);
    let Y = date.getFullYear() + '-';
    let M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    let D = date.getDate();
    return Y + M + D;
  },
  showNext(e){
    console.log(this.data.show)
    const index = e.currentTarget.dataset.index;
    let posts = this.data.posts;
    this.setData({
      show: true,
      post: posts[index],
      transformIdx: index
    })
    WxParse.wxParse('article_content', 'html', posts[index].content, this, 0)
    console.log(this.data.show)
  },
  showPrev() {
    this.setData({
      show: false
    })
  }
})