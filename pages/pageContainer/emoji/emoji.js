import { emojiMap, emojiName, emojiUrl } from '../../../utils/emojiMap'
const emojiNames = '[微笑]|[撇嘴]|[色]|[发呆]|[得意]|[流泪]|[害羞]|[闭嘴]|[睡]|[大哭]|[尴尬]|[发怒]|[调皮]|[呲牙]|[惊讶]|[难过]|[酷]|[冷汗]|[抓狂]|[吐]|[偷笑]|[愉快]|[白眼]|[傲慢]|[饥饿]|[困]|[惊恐]|[流汗]|[憨笑]|[悠闲]|[奋斗]|[咒骂]|[疑问]|[嘘]|[晕]|[疯了]|[衰]|[骷髅]|[敲打]|[再见]|[擦汗]|[抠鼻]|[鼓掌]|[糗大了]|[坏笑]|[左哼哼]|[右哼哼]|[哈欠]|[鄙视]|[委屈]|[快哭了]|[阴险]|[亲亲]|[吓]|[可怜]|[菜刀]|[西瓜]|[啤酒]|[篮球]|[乒乓]|[咖啡]|[饭]|[猪头]|[玫瑰]|[凋谢]|[嘴唇]|[爱心]|[心碎]|[蛋糕]|[闪电]|[炸弹]|[刀]|[足球]|[瓢虫]|[便便]|[月亮]|[太阳]|[礼物]|[拥抱]|[强]|[弱]|[握手]|[胜利]|[抱拳]|[勾引]|[拳头]|[差劲]|[爱你]|[NO]|[OK]|[爱情]|[飞吻]|[跳跳]|[发抖]|[怄火]|[转圈]|[磕头]|[回头]|[跳绳]|[投降]|[激动]|[乱舞]|[献吻]|[左太极]|[右太极]|[嘿哈]|[捂脸]|[奸笑]|[机智]|[皱眉]|[耶]|[茶]|[红包]|[蜡烛]|[福]|[鸡]|[笑脸]|[生病]|[破涕为笑]|[吐舌]|[脸红]|[恐惧]|[失望]|[无语]|[鬼魂]|[合十]|[强壮]|[庆祝]|[礼物]|[囧]|[再见]|[抱拳]|[皱眉]'.split('|');
const animation = wx.createAnimation({
  duration: 400,
  timingFunction: 'ease-out',
  delay: 0,
  transformOrigin: '50% 50% 0'
});
Page({
  data: {
    collapse: false,
    message: '',
    animation: ''
  },

  onLoad: function (options) {

  },

  chooseEmoji(e){
    console.log("chooseEmoji",e.detail.data);
    this.setData({
      message : this.data.message + e.detail.data
    })
  },
  ChangeCollapse() {
    let collapse = !this.data.collapse;
    if (collapse == true) {
      this.start_animation();
    } else {
      this.end_animation();
    }
  },
  start_animation(){
    animation.height('400rpx').step()
    this.setData({
      animation: animation.export(),
      collapse: true
    })
  },
  end_animation(){
    animation.height('100rpx').step()
    this.setData({animation: animation.export()})
    setTimeout(()=>{
      this.setData({
        collapse : false
      })
    },400)
  },
    // 解析文本信息
    // textFormat(item) {
    //   console.log("item",item)
    //   if(item.payload.name == 'text'){
    //     return item.payload;
    //   }
    //   if(item.type != 'TIMTextElem' || item.payload.text == null){
    //     return item.payload;
    //   }
    //   let renderDom = []
    //    let temp = item.payload.text;
    //    let left = -1;
    //    let right = -1;
    //    while (temp !== '') {
    //      left = temp.indexOf('[')
    //      right = temp.indexOf(']')
    //      switch (left) {
    //        case 0:
    //          if (right === -1) {
    //            renderDom.push({
    //              name: 'text',
    //              text: temp
    //            })
    //            temp = ''
    //          } else {
    //            let _emoji = temp.slice(0, right + 1)
    //            if (emojiMap[_emoji]) {
    //              renderDom.push({
    //                name: 'img',
    //                src: emojiUrl + emojiMap[_emoji]
    //              })
    //              temp = temp.substring(right + 1)
    //            } else {
    //              renderDom.push({
    //                name: 'text',
    //                text: '['
    //              })
    //              temp = temp.slice(1)
    //            }
    //          }
    //          break
    //        case -1:
    //          renderDom.push({
    //            name: 'text',
    //            text: temp
    //          })
    //          temp = ''
    //          break
    //        default:
    //          renderDom.push({
    //            name: 'text',
    //            text: temp.slice(0, left)
    //          })
    //          temp = temp.substring(left)
    //          break
    //      }
    //    }
    //    console.log("renderDom",renderDom)
    //   return renderDom;
    // },
})