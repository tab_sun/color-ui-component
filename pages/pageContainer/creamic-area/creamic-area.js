// pages/component/creamic-area/creamic-area.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ColorList: [{
      title: '1',
      name: 'red',
      color: '#e54d42',
    },
    {
      title: '2',
      name: 'orange',
      color: '#f37b1d',
    },
    {
      title: '3',
      name: 'green',
      color: '#fbbd08',
    },
    {
      title: '4',
      name: 'blue',
      color: '#8dc63f',
    },
  ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})