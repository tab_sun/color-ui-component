const app = getApp();
Page({
  data: {
    alllist: [{
      value: '',
      type: 'text'
    }],
    idx: 0, //暂存长按的下标
  },
  onLoad() {

  },
  chooseImage(e) {
    wx.chooseImage({
      sizeType: ['original', 'compressed'], //可选择原图或压缩后的图片
      sourceType: ['album', 'camera'], //可选择性开放访问相册、相机
      success: res => {
        let data = res.tempFilePaths;
        data = data.map(item => {
          return {
            value: item,
            type: 'image'
          }
        })
        console.log(data)
        this.setData({
          alllist: [...this.data.alllist, ...data]
        })
        console.log(this.data.alllist);
      }
    })
  },
  handleList() {
    this.setData({
      alllist: [...this.data.alllist, {
        value: '',
        type: 'text'
      }]
    })
    console.log(this.data.alllist)
  },
  setText(e) {
    let value = e.detail.value;
    const idx = e.currentTarget.dataset.idx;
    let alllist = this.data.alllist;
    alllist[idx].value = value;
    this.setData({
      alllist
    })
  },
  remove(e) {
    console.log(e);
    const idx = e.currentTarget.dataset.idx;
    let alllist = this.data.alllist;
    alllist.splice(idx, 1);
    this.setData({
      alllist
    })
    console.log(this.data.alllist);
  },
  handleImagePreview(e) {
    const idx = e.target.dataset.idx
    const images = this.data.images
    wx.previewImage({
      current: images[idx],
      urls: images
    })
  },
  save() {
    console.log(this.data.alllist);
  },
  show(e) {
    if(this.data.alllist.length == 1){
      return;
    }
    let idx = e.currentTarget.dataset.idx;
    this.setData({
      show: true,
      idx
    })
  },
  moveUp(){
    let idx = this.data.idx;
    let alllist = this.data.alllist;
    alllist = this.exChange(alllist, idx, idx - 1);
    console.log(alllist,idx)
    this.setData({
      alllist,
      show: false,
    })
  },
  moveDown(){
    let idx = this.data.idx;
    let alllist = this.data.alllist;
    console.log(alllist,idx)
    alllist = this.exChange(alllist, idx, idx + 1);
    console.log(alllist,idx)
    this.setData({
      alllist,
      show: false,
    })
  },
  exChange(arr, index1, index2) {
    arr[index1] = arr.splice(index2, 1, arr[index1])[0]
    return arr;
  }
})