Page({
  data: {
    list: [{
        name: '这里是标题鸭',
        content: ['芜湖~ 芜湖~','芜湖~ 芜湖~ 芜湖~'],
        checked: false
      },
      {
        name: '这里是第二个标题鸭',
        content: ['芜湖~ 芜湖~','芜湖~'],
        checked: false
      },
      {
        name: '这里是第三个标题鸭',
        content: ['芜湖~ 芜湖~','芜湖~ 芜湖~'],
        checked: false
      },
    ],
  },

  onLoad: function (options) {},
  // 点击列表,收缩与展示
  click(event) {
    const index = event.currentTarget.dataset.index;
    const {
      list
    } = this.data;
    list.forEach(v=>{
      v.checked = false
    })
    list[index].checked = true
    this.setData({
      list
    });
  },
});