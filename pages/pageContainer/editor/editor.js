Page({
  data: {
    _id: "",
    content: '',
    keyboardHeight: 0,
  },
  format(e) {
    let { name, value } = e.target.dataset
    if (!name) return
    this.editorCtx.format(name, value)
  },
  changeEditor(e) {
    const {
      html,
      text,
      delta
    } = e.detail;
    console.log(html)
    this.setData({
      content: html
    })
    console.log(this.data.content)
  },
  async onLoad() {
    this.onEditorReady();
  },

  onEditorReady() {
    const that = this;
    wx.createSelectorQuery().in(this).select('#editor').context(function (res) {
      that.editorCtx = res.context;
      that.editorCtx.setContents({
        html: that.data.content,
        success: function (res) {
          console.log(res);
          console.log('富文本加载成功')
        }
      })
    }).exec()
  },
  finish() {
    console.log(this.data.content)
  },
  insertImage() {
    this.editorCtx.insertImage({
      src: "填服务器返回的图片路径",
      width: '100%',
      success: (res) => {
        console.log(res)
      }
    })
  }
})